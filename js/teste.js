function somarValores(a, b) {
    return a + b;
}

function subtrairValores(a, b) {
    return a - b;
}

function multiplicandoValores(a, b) {
    return a * b;
}

function dividirValores(a, b) {
    return a / b;
}

function imparOuPar(total) {
    return (total % 2) == 0 ? "par" : "impar"
}

var soma = parseInt(somarValores(1,10));
var divide = parseInt(dividirValores(5,3));
var multiplica = parseInt(multiplicandoValores(4,6));
var subtrair = parseInt(subtrairValores(8,2));


console.log(`a soma dos numeros é ${soma} e ele também é ${imparOuPar(soma)}`);
console.log(`a subtração dos numeros é ${subtrair} e ele também é ${imparOuPar(subtrair)}`);
console.log(`a multiplicação dos numeros é ${multiplica} e ele também é ${imparOuPar(multiplica)}`);
console.log(`a divisão dos numeros é ${divide} e ele também é ${imparOuPar(divide)}`);