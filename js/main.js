const canvas = document.getElementById("myCanvas");
const context = canvas.getContext("2d");
var audio = document.getElementById("myAudio");

context.canvas.width = window.innerWidth - 20;
context.canvas.height = window.innerHeight - 20;

const spriteEnemy = new Image();
spriteEnemy.src = "assets/sprites/enemyPrisoner.png";
const spriteInnocent = new Image();
spriteInnocent.src = "assets/sprites/npc.png";
const spriteInnocent2 = new Image();
spriteInnocent2.src = "assets/sprites/npc.png";
const spriteInnocent3 = new Image();
spriteInnocent3.src = "assets/sprites/npc.png";
const spriteCar2 = new Image();
spriteCar2.src = "assets/images/car2.png";
const spriteCar3 = new Image();
spriteCar3.src = "assets/images/car3.png";
const spriteTower = new Image();
spriteTower.src = "assets/sprites/tower.png";
const spriteTree = new Image();
spriteTree.src = "assets/sprites/treeRock.png";
const spriteFence = new Image();
spriteFence.src = "assets/images/fence.png";
const spriteFence2 = new Image();
spriteFence2.src = "assets/sprites/fence2.png";
const spriteRoad = new Image();
spriteRoad.src = "assets/sprites/Road_test.png";
const spriteGround = new Image();
spriteGround.src = "assets/sprites/concreteWalls.png";
audioEnemy = new Audio("assets/sfx/audioEnemy.wav");
audioInnocent = new Audio("assets/sfx/audioInnocent.wav");
audioGun = new Audio("assets/sfx/shootGun.wav");
audioImpactCar = new Audio("assets/sfx/impactCar.wav");
audioGun2 = new Audio("assets/sfx/gunShot2.wav");

let frames = 0;
var score = 0;
var contKillEnemy = 0;
var arrayInnocents = [];
var arrayEnemy = [];

var numberOfEnemys = 3;
var maxNumberOfEnemys = 15;
var numberOfInnocents = 2;

var gameOver = false;
var startPos = true;

var now = new Date().getTime();

var velocityEnemy = 3;
var isMainMenuDiffcult = true;
var timeIntervalSpawnEnemy = 3000;

function makeEnemy() {
  for (let i = 0; i < numberOfEnemys; i++) {
    arrayEnemy.push({
      x: Math.round(Math.random() * 100) + 20,
      y: Math.round(Math.random() * 150) + 350,
      positionBegin: [
        [{ posX: 0, posY: 0, height: 31, width: 35 }],

        [{ posX: 0, posY: 41, height: 31, width: 36 }],

        [{ posX: 0, posY: 85, height: 29, width: 33 }],
      ],

      enemyIndex: 0,
      velocity: velocityEnemy,
      countEnemy: 0,

      movimentsAlive: [
        [
          { posX: 0, posY: 0 },
          { posX: 35, posY: 0 },
          { posX: 70, posY: 0 },
        ],

        [
          { posX: 0, posY: 0 },
          { posX: 35, posY: 0 },
          { posX: 70, posY: 0 },
        ],

        [
          { posX: 0, posY: 0 },
          { posX: 35, posY: 0 },
          { posX: 70, posY: 0 },
        ],
      ],
      currentFrame: 0,
      isAlive: true,
    });
  }
}

function drawEnemy() {
  arrayEnemy.forEach(function (objEnemy, i) {
    const { posX, posY } =
      objEnemy.movimentsAlive[objEnemy.enemyIndex][
        objEnemy.currentFrame % objEnemy.movimentsAlive.length
      ];

    if (objEnemy.isAlive == true) {
      context.drawImage(
        spriteEnemy,
        posX,
        posY,
        objEnemy.positionBegin[objEnemy.enemyIndex][objEnemy.enemyIndex].width,
        objEnemy.positionBegin[objEnemy.enemyIndex][objEnemy.enemyIndex].height,
        objEnemy.x,
        objEnemy.y,
        objEnemy.positionBegin[objEnemy.enemyIndex][objEnemy.enemyIndex].width,
        objEnemy.positionBegin[objEnemy.enemyIndex][objEnemy.enemyIndex].height
      );
    }
  });
}

function updateEnemy() {
  arrayEnemy.forEach(function (objEnemy, i) {
    this.currentFrame = this.currentFrame > 1000 ? 0 : this.currentFrame;
    const passedTheBreak = frames % 5 === 0;
    if (passedTheBreak) {
      let menorDiffX = 10000;
      let menorDiffY = 10000;

      arrayInnocents.forEach(function (objInnocent, i) {
        var diffX = objInnocent.x - objEnemy.x;
        var diffY = objInnocent.y - objEnemy.y;

        if (diffX < menorDiffX && diffY < menorDiffY) {
          menorDiffX = diffX;
          menorDiffY = diffY;
        }
      });

      if (menorDiffX > 0) {
        objEnemy.x += velocityEnemy;
      } else {
        objEnemy.x -= velocityEnemy;
      }

      if (menorDiffY > 0) {
        objEnemy.y += velocityEnemy;
      } else {
        objEnemy.y -= velocityEnemy;
      }

      objEnemy.currentFrame++;
    }

    if (objEnemy.x > canvas.width) {
      objEnemy.x = -50;
    }
  });
}

function drawInnocent() {
  arrayInnocents.forEach(function (objInnocent, i) {
    if (objInnocent.isAlive == true) {
      const { posX, posY } =
        objInnocent.movimentsAlive[objInnocent.indexMovimentsAlive][
          objInnocent.currentFrame % objInnocent.movimentsAlive.length
        ];

      context.drawImage(
        spriteInnocent,
        posX,
        posY,
        objInnocent.width,
        objInnocent.height,
        objInnocent.x,
        objInnocent.y,
        objInnocent.width,
        objInnocent.height
      );
    }
  });
}

function updateInnocent() {
  arrayInnocents.forEach(function (objInnocent, i) {
    this.currentFrame = this.currentFrame > 1000 ? 0 : this.currentFrame;
    const passedTheBreak = frames % 5 === 0;
    if (passedTheBreak) {
      objInnocent.currentFrame++;
      objInnocent.y = objInnocent.y + objInnocent.velocity;
    }
    if (objInnocent.y > canvas.height) {
      objInnocent.y = -20;
    }
  });
}

function makeInnocents() {
  for (let i = 0; i < numberOfInnocents; i++) {
    // objInnocent.y = Math.round(Math.random(1,1000) * 1000);
    objInnocent = {
      posX: 0,
      posY: 0,
      height: 19,
      width: 16,
      x: 1000,
      y: Math.round(Math.random(1, 1000) * 1000),
      velocity: 1,
      indexMovimentsAlive: 0,
      movimentsAlive: [
        [
          { posX: 0, posY: 18 },
          { posX: 18, posY: 18 },
          { posX: 36, posY: 18 },
        ],
        [
          { posX: 0, posY: 38 },
          { posX: 18, posY: 38 },
          { posX: 36, posY: 38 },
        ],
      ],
      currentFrame: 0,
      isAlive: true,
    };
    arrayInnocents.push({ ...objInnocent });
  }

  for (let i = 0; i < numberOfInnocents; i++) {
    // objInnocent.y = Math.round(Math.random(1,1000) * 1000);
    objInnocent = {
      posX: 0,
      posY: 0,
      height: 19,
      width: 16,
      x: 1250,
      y: Math.round(Math.random(1, 1000) * 1000),
      velocity: 2,
      indexMovimentsAlive: 0,
      movimentsAlive: [
        [
          { posX: 0, posY: 18 },
          { posX: 18, posY: 18 },
          { posX: 36, posY: 18 },
        ],
        [
          { posX: 0, posY: 38 },
          { posX: 18, posY: 38 },
          { posX: 36, posY: 38 },
        ],
      ],
      currentFrame: 0,
      isAlive: true,
    };
    arrayInnocents.push({ ...objInnocent });
  }
}

var objCar2 = {
  x: 1150,
  y: -100,
  velocity: 7,
  currentFrame: 0,

  update: function () {
    this.currentFrame = this.currentFrame > 1000 ? 0 : this.currentFrame;
    const passedTheBreak = frames % 2 === 0;
    if (passedTheBreak) {
      objCar2.currentFrame++;
      objCar2.y = objCar2.y + objCar2.velocity;
    }
    if (objCar2.y > canvas.height) {
      objCar2.y = -100;
    }
  },
  draw: function () {
    context.drawImage(spriteCar2, objCar2.x, objCar2.y);
  },
};

var objCar3 = {
  x: 1050,
  y: canvas.height + 100,
  velocity: 7,
  currentFrame: 0,

  update: function () {
    this.currentFrame = this.currentFrame > 1000 ? 0 : this.currentFrame;
    const passedTheBreak = frames % 2 === 0;
    if (passedTheBreak) {
      objCar3.currentFrame++;
      objCar3.y = objCar3.y - objCar3.velocity;
    }
    if (objCar3.y < -100) {
      objCar3.y = canvas.height + 100;
    }
  },
  draw: function () {
    context.drawImage(spriteCar3, objCar3.x, objCar3.y);
  },
};

var objTower = {
  posX: 88,
  posY: 32,
  height: 205,
  width: 94,

  towerPosition: [
    { x: 0, y: 0 },
    { x: 0, y: 450 },
    { x: 450, y: 0 },
    { x: 450, y: 450 },
  ],
  currentFrame: 0,

  update: function () {
    const passedTheBreak = frames % 5 === 0;
    if (passedTheBreak) {
      objTower.currentFrame++;
    }
  },
  draw: function () {
    for (let i = 0; i < objTower.towerPosition.length; i++) {
      context.drawImage(
        spriteTower,
        objTower.posX,
        objTower.posY,
        objTower.width,
        objTower.height,
        objTower.towerPosition[i].x,
        objTower.towerPosition[i].y,
        objTower.width,
        objTower.height
      );
    }
  },
};

var objFence = {
  posX: 6,
  posY: 11,
  height: 56,
  width: 147,

  fencePosition: [
    { x: 50, y: 140 },
    { x: 195, y: 140 },
    { x: 340, y: 140 },
    { x: 50, y: 560 },
    { x: 195, y: 560 },
    { x: 340, y: 560 },
  ],
  currentFrame: 0,

  update: function () {
    const passedTheBreak = frames % 5 === 0;
    if (passedTheBreak) {
      objFence.currentFrame++;
    }
  },
  draw: function () {
    for (let i = 0; i < objFence.fencePosition.length; i++) {
      context.drawImage(
        spriteFence,
        objFence.posX,
        objFence.posY,
        objFence.width,
        objFence.height,
        objFence.fencePosition[i].x,
        objFence.fencePosition[i].y,
        objFence.width,
        objFence.height
      );
    }
  },
};

var objFence2 = {
  posX: 300,
  posY: 185,
  height: 85,
  width: 4,

  fencePositionTopDown: [
    { x: 25, y: 140 },
    { x: 25, y: 200 },
    { x: 25, y: 260 },
    { x: 25, y: 320 },
    { x: 25, y: 380 },
    { x: 25, y: 440 },
    { x: 25, y: 500 },
    { x: 25, y: 560 },

    { x: 515, y: 140 },
    { x: 515, y: 200 },
    { x: 515, y: 260 },
    // {x: 515, y: 320},
    // {x: 515, y: 380},
    { x: 515, y: 440 },
    { x: 515, y: 500 },
    { x: 515, y: 560 },
  ],
  currentFrame: 0,

  update: function () {
    const passedTheBreak = frames % 5 === 0;
    if (passedTheBreak) {
      objFence2.currentFrame++;
    }
  },
  draw: function () {
    for (let i = 0; i < objFence2.fencePositionTopDown.length; i++) {
      context.drawImage(
        spriteFence2,
        objFence2.posX,
        objFence2.posY,
        objFence2.width,
        objFence2.height,
        objFence2.fencePositionTopDown[i].x,
        objFence2.fencePositionTopDown[i].y,
        objFence2.width,
        objFence2.height
      );
    }
  },
};

var objGround1 = {
  posX: 179,
  posY: 35,
  height: 38,
  width: 41,

  groundPosition: [
    { x: 25, y: 195 },
    { x: 25, y: 230 },
    { x: 25, y: 265 },
    { x: 25, y: 300 },
    { x: 25, y: 335 },
    { x: 25, y: 370 },
    { x: 25, y: 405 },
    { x: 25, y: 440 },
    { x: 25, y: 475 },
    { x: 25, y: 510 },
    { x: 25, y: 545 },
    { x: 25, y: 580 },
    { x: 25, y: 615 },

    { x: 65, y: 195 },
    { x: 65, y: 230 },
    { x: 65, y: 265 },
    { x: 65, y: 300 },
    { x: 65, y: 335 },
    { x: 65, y: 370 },
    { x: 65, y: 405 },
    { x: 65, y: 440 },
    { x: 65, y: 475 },
    { x: 65, y: 510 },
    { x: 65, y: 545 },
    { x: 65, y: 580 },
    { x: 65, y: 615 },

    { x: 105, y: 195 },
    { x: 105, y: 230 },
    { x: 105, y: 265 },
    { x: 105, y: 300 },
    { x: 105, y: 335 },
    { x: 105, y: 370 },
    { x: 105, y: 405 },
    { x: 105, y: 440 },
    { x: 105, y: 475 },
    { x: 105, y: 510 },
    { x: 105, y: 545 },
    { x: 105, y: 580 },
    { x: 105, y: 615 },

    { x: 145, y: 195 },
    { x: 145, y: 230 },
    { x: 145, y: 265 },
    { x: 145, y: 300 },
    { x: 145, y: 335 },
    { x: 145, y: 370 },
    { x: 145, y: 405 },
    { x: 145, y: 440 },
    { x: 145, y: 475 },
    { x: 145, y: 510 },
    { x: 145, y: 545 },
    { x: 145, y: 580 },
    { x: 145, y: 615 },

    { x: 185, y: 195 },
    { x: 185, y: 230 },
    { x: 185, y: 265 },
    { x: 185, y: 300 },
    { x: 185, y: 335 },
    { x: 185, y: 370 },
    { x: 185, y: 405 },
    { x: 185, y: 440 },
    { x: 185, y: 475 },
    { x: 185, y: 510 },
    { x: 185, y: 545 },
    { x: 185, y: 580 },
    { x: 185, y: 615 },

    { x: 225, y: 195 },
    { x: 225, y: 230 },
    { x: 225, y: 265 },
    { x: 225, y: 300 },
    { x: 225, y: 335 },
    { x: 225, y: 370 },
    { x: 225, y: 405 },
    { x: 225, y: 440 },
    { x: 225, y: 475 },
    { x: 225, y: 510 },
    { x: 225, y: 545 },
    { x: 225, y: 580 },
    { x: 225, y: 615 },

    { x: 265, y: 195 },
    { x: 265, y: 230 },
    { x: 265, y: 265 },
    { x: 265, y: 300 },
    { x: 265, y: 335 },
    { x: 265, y: 370 },
    { x: 265, y: 405 },
    { x: 265, y: 440 },
    { x: 265, y: 475 },
    { x: 265, y: 510 },
    { x: 265, y: 545 },
    { x: 265, y: 580 },
    { x: 265, y: 615 },

    { x: 305, y: 195 },
    { x: 305, y: 230 },
    { x: 305, y: 265 },
    { x: 305, y: 300 },
    { x: 305, y: 335 },
    { x: 305, y: 370 },
    { x: 305, y: 405 },
    { x: 305, y: 440 },
    { x: 305, y: 475 },
    { x: 305, y: 510 },
    { x: 305, y: 545 },
    { x: 305, y: 580 },
    { x: 305, y: 615 },

    { x: 345, y: 195 },
    { x: 345, y: 230 },
    { x: 345, y: 265 },
    { x: 345, y: 300 },
    { x: 345, y: 335 },
    { x: 345, y: 370 },
    { x: 345, y: 405 },
    { x: 345, y: 440 },
    { x: 345, y: 475 },
    { x: 345, y: 510 },
    { x: 345, y: 545 },
    { x: 345, y: 580 },
    { x: 345, y: 615 },

    { x: 385, y: 195 },
    { x: 385, y: 230 },
    { x: 385, y: 265 },
    { x: 385, y: 300 },
    { x: 385, y: 335 },
    { x: 385, y: 370 },
    { x: 385, y: 405 },
    { x: 385, y: 440 },
    { x: 385, y: 475 },
    { x: 385, y: 510 },
    { x: 385, y: 545 },
    { x: 385, y: 580 },
    { x: 385, y: 615 },

    { x: 425, y: 195 },
    { x: 425, y: 230 },
    { x: 425, y: 265 },
    { x: 425, y: 300 },
    { x: 425, y: 335 },
    { x: 425, y: 370 },
    { x: 425, y: 405 },
    { x: 425, y: 440 },
    { x: 425, y: 475 },
    { x: 425, y: 510 },
    { x: 425, y: 545 },
    { x: 425, y: 580 },
    { x: 425, y: 615 },

    { x: 465, y: 195 },
    { x: 465, y: 230 },
    { x: 465, y: 265 },
    { x: 465, y: 300 },
    { x: 465, y: 335 },
    { x: 465, y: 370 },
    { x: 465, y: 405 },
    { x: 465, y: 440 },
    { x: 465, y: 475 },
    { x: 465, y: 510 },
    { x: 465, y: 545 },
    { x: 465, y: 580 },
    { x: 465, y: 615 },
  ],
  currentFrame: 0,

  update: function () {
    const passedTheBreak = frames % 5 === 0;
    if (passedTheBreak) {
      objGround1.currentFrame++;
    }
  },
  draw: function () {
    for (let i = 0; i < objGround1.groundPosition.length; i++) {
      context.drawImage(
        spriteGround,
        objGround1.posX,
        objGround1.posY,
        objGround1.width,
        objGround1.height,
        objGround1.groundPosition[i].x,
        objGround1.groundPosition[i].y,
        objGround1.width,
        objGround1.height
      );
    }
  },
};

var objRoad = {
  posX: 192,
  posY: 330,
  height: 514,
  width: 355,

  roadPosition: [
    { x: 950, y: 0 },
    { x: 950, y: 505 },
  ],

  currentFrame: 0,

  update: function () {
    const passedTheBreak = frames % 5 === 0;
    if (passedTheBreak) {
      objRoad.currentFrame++;
    }
  },
  draw: function () {
    for (let i = 0; i < objRoad.roadPosition.length; i++) {
      context.drawImage(
        spriteRoad,
        objRoad.posX,
        objRoad.posY,
        objRoad.width,
        objRoad.height,
        objRoad.roadPosition[i].x,
        objRoad.roadPosition[i].y,
        objRoad.width,
        objRoad.height
      );
    }
  },
};

// var objTree = {
//     posX: 75,
//     posY: 125,
//     height: 75,
//     width: 70,
//     x: 850,
//     y: 550,
//     currentFrame: 0,

//     update: function(){
//         const passedTheBreak = frames % 5 === 0;
//         if(passedTheBreak){
//             objTree.currentFrame++;
//         }
//     },
//     draw: function(){
//         context.drawImage(
//             spriteTree,
//             objTree.posX, objTree.posY,
//             objTree.width, objTree.height,
//             objTree.x, objTree.y,
//             objTree.width, objTree.height,
//         )
//     }
// }

function collision(mx, my, hx, hy) {
  if (
    ((mx > hx && mx < hx + 35) || (mx < hx && mx > hx - 35)) &&
    ((my > hy && my < hy + 31) || (my < hy && my > hy - 31))
  ) {
    return true;
  }
  return false;
}

function collisionDifficultText(mx, my){
  if ( ( (mx >= 200 && mx <= 1070) && (my >= 350 && my <=430) ) ){
      return true;
  }
//   if ( ( (mx >= 500 && mx <= 1375) && (my >= 550 && my <=630) ) ){
//       return true;
//   }
  return false;
}


function setDiffcultText(event) {
  if (collisionDifficultText(event.x, event.y)) {
    var selectLevel = selectCollisionDiffcultlevel(event.x);

    switch (selectLevel) {
      case "easy":
        canvas.removeEventListener("click", setDiffcultText);
        isMainMenuDiffcult = false;
        intervalTimeOut();
        numberOfEnemys = 1;
        velocityEnemy = 1;
        timeIntervalSpawnEnemy = 5000;
        canvas.classList.add("cursor");
        break;
      case "normal":
        canvas.removeEventListener("click", setDiffcultText);
        isMainMenuDiffcult = false;
        intervalTimeOut();
        numberOfEnemys = 5;
        velocityEnemy = 3;
        timeIntervalSpawnEnemy = 3000;
        canvas.classList.add("cursor");
        break;
      case "hard":
        canvas.removeEventListener("click", setDiffcultText);
        isMainMenuDiffcult = false;
        intervalTimeOut();
        numberOfEnemys = 10;
        velocityEnemy = 10;
        timeIntervalSpawnEnemy = 1000;
        canvas.classList.add("cursor");
        break;
      default:
        break;
    }
  }
}

function intervalTimeOut() {
  setInterval(function () {
    if (!gameOver) {
      makeEnemy();
    }
  }, timeIntervalSpawnEnemy);

  setTimeout(function () {
    canvas.addEventListener("click", checkCollisionEnemy);
  }, 5000);
}

canvas.addEventListener("click", setDiffcultText);

function selectCollisionDiffcultlevel(mx){
  if ( ( (mx >= 200 && mx <= 375) ) ){
      return "easy";
  } else if ( (mx >= 465 && mx <= 750) ){
      return "normal";
  } else if( (mx >= 890 && mx <= 1067) ){
      return "hard";
  } else{
      return "nenhum";
  }

//   if ( ( (mx >= 500 && mx <= 685) ) ){
//       return "easy";
//   } else if ( (mx >= 755 && mx <= 1065) ){
//       return "normal";
//   } else if( (mx >= 1200 && mx <= 1375) ){
//       return "hard";
//   } else{
//       return "none";
//   }
}

function collisionInnocents() {
  arrayEnemy.forEach(function (objEnemy, i) {
    arrayInnocents.forEach(function (objInnocents, j) {
      if (collision(objInnocents.x, objInnocents.y, objEnemy.x, objEnemy.y)) {
        audioInnocent.play();
        arrayInnocents.splice(j, 1);
      }
    });
    if (!arrayInnocents.length > 0) {
      arrayInnocents.isAlive = false;
      gameOver = true;
      gameOverText();
      canvas.removeEventListener("click", checkCollisionEnemy);
      canvas.style.cursor = "auto";
    }
  });
}

function checkCollisionEnemy(e) {
  audioGun2.play();

  arrayEnemy.forEach(function (objEnemy, i) {
    if (collision(e.x, e.y, objEnemy.x, objEnemy.y)) {
      audioEnemy.play();
      arrayEnemy.splice(i, 1);
      score += 10;
    }

    if (!arrayEnemy.length > 0) {
      arrayEnemy.isAlive = false;
      gameOver = true;
      gameWinText();
      canvas.removeEventListener("click", checkCollisionEnemy);
      canvas.style.cursor = "auto";
    }
  });
}

const playAudioInnocent = () => audioInnocent.play();
const playAudioEnemy = () => audioEnemy.play();
const playAudioImpactCar = () => audioImpactCar.play();



function gameOverText() {
  context.beginPath();
  context.fillStyle = "rgb(0, 0, 0, 1)";
  context.fillRect(0, 0, canvas.width, canvas.height);
  context.beginPath();
  context.fillStyle = "red";
  context.font = "100px creepster";
  context.textAlign = "center";
  context.textBaseline = "middle";
  context.fillText("GAME OVER", canvas.width / 2, canvas.height / 2);
  context.fillText(
    `Sua pontuação: ${score} pontos`,
    canvas.width / 2,
    canvas.height / 1.5
  );
}

function gameWinText() {
  context.beginPath();
  context.fillStyle = "rgba(0, 0, 0, 1)";
  context.fillRect(0, 0, canvas.width, canvas.height);
  context.beginPath();
  context.fillStyle = "crimson";
  context.font = "50px creepster";
  context.textAlign = "center";
  context.textBaseline = "middle";
  context.fillText(
    "PARABÉNS! VOCÊ MATOU TODOS OS BANDIDOS",
    canvas.width / 2,
    canvas.height / 2
  );
  context.fillText(
    "Sua pontuação: " + score + " pontos ",
    canvas.width / 2,
    canvas.height / 1.5
  );
}

function renderScore() {
  context.beginPath();
  context.fillStyle = "black";
  context.font = "30px roboto mono";
  context.textAlign = "center";
  context.textBaseline = "middle";
  context.fillText("Score " + score, 250, 20);
}

function tutorialText() {
  context.beginPath();
  context.fillStyle = "rgb(0, 0, 0, 0.5)";
  context.fillRect(0, 0, canvas.width, canvas.height);
  context.beginPath();
  context.fillStyle = "red";
  context.font = "80px creepster";
  context.textAlign = "center";
  context.textBaseline = "middle";
  context.fillText(
    "NEUTRALIZE OS INIMIGOS ATIRANDO NELES",
    canvas.width / 2,
    canvas.height / 2
  );
}

function diffcultText() {
  context.beginPath();
  context.fillStyle = "rgb(0, 0, 0, 0.25)";
  context.fillRect(0, 0, canvas.width, canvas.height);
  context.beginPath();
  context.fillStyle = "red";
  context.font = "100px creepster";
  context.textAlign = "center";
  context.textBaseline = "middle";
  context.fillText(
    "Selecione a dificuldade desejada",
    canvas.width / 2,
    canvas.height / 3.5
  );
  context.fillText(
    "Easy    Normal      Hard",
    canvas.width / 2,
    canvas.height / 1.5
  );
}

function loop() {
  if (!gameOver) {
    if (isMainMenuDiffcult) {
      context.clearRect(0, 0, canvas.width, canvas.height);
      diffcultText();
    } else {
      context.clearRect(0, 0, canvas.width, canvas.height);
      objRoad.draw();
      objRoad.update();

      objGround1.draw();
      objGround1.update();

      objFence.draw();
      objFence.update();

      objFence2.draw();
      objFence2.update();

      // objTree.draw();
      // objTree.update();

      objTower.draw();
      objTower.update();

      if (startPos) {
        makeInnocents();
        makeEnemy();
        startPos = false;
      }

      drawInnocent();
      updateInnocent();

      drawEnemy();
      updateEnemy();

      objCar3.draw();
      objCar3.update();

      objCar2.draw();
      objCar2.update();

      collisionInnocents();

      collisionDifficultText;

      renderScore(score);

      let dateLimit = new Date().getTime();

      if (dateLimit - now < 5000) {
        context.clearRect(0, 0, canvas.width, canvas.height);
        tutorialText();
      }
    }

    frames += 1;

    requestAnimationFrame(loop);
  }
}
loop();
